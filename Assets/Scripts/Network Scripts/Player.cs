﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Player : BaseHealth {//BaseWeapon {// : NetworkBehaviour {

	[SyncVar]
	private bool _isDead = false; // this shows if the player is dead
	public bool isDead // this is an accessor 
	{
		get { return _isDead; } //anything can check if the player is dead
		protected set { _isDead = value; } //only player class or those that derive from it can change it
	}

	/* MOVED to parent*/
	//[SerializeField]
	//private int maxHealth = 100; // i might move this to a base health system later 

	// to mark as sync variable use this
	//[SyncVar] //everytime value changes it will be pushed to all clients :D
	//private int currentHealth; //NEEDs to be synced across all clients 


	[SerializeField]
	private Behaviour[] disableOnDeath;

	private bool[] wasEnabled; 


	public void Setup() // player setup is called from the playersetip script
	{
		wasEnabled = new bool[disableOnDeath.Length];
		for (int i = 0; i < wasEnabled.Length; i++) {
			wasEnabled [i] = disableOnDeath [i].enabled;
			//this simply renables all the componentes that were disabled on death
		}

		setDefaults (); 
	}

	public void setDefaults()
	{
		currentHealth = maxHealth;

		for (int i = 0; i < disableOnDeath.Length; i++) {
			disableOnDeath [i].enabled = wasEnabled[i];
			// loop through all the componenets and state weather they were enabled orginally
			// need speical case for colliders since they are not behavious.
			// colliders are derived from components which behaviours derived from. 
		}

		//special case to handle the collider 
		Collider col = GetComponent<Collider> ();
		if (col != null) {
			col.enabled = true;
		}
	}

	/*void Update()
	{		
		if (!isLocalPlayer)
			return;

		//this is test code to see if I can kill the player properly 
		if(Input.GetKeyDown(KeyCode.K))
		{
			RpcTakeDamage (9999); //ITS OVER 9000!
		}

	}// */
		

	//the player will locally apply their damage
	//[ClientRpc] // rpc ensure that all computers on the network carries out function 
	[ClientRpc]
	public void RpcTakeDamage(int _amount)
	{
		//only take damage if not dead
		if (isDead) {
			return;
		}

		//since currenthealth is a sync var
		currentHealth -= _amount; // it will automatically be synced. 
		Debug.Log(transform.name + " now has " + currentHealth + " health"); 

		if (currentHealth <= 0) {
			Die ();
		}

	}

	//private so player can only die through take damage method. 
	private void Die()
	{
		isDead = true;

		//Disable components 
		Debug.Log(transform.name + "is dead"); 

		for (int i = 0; i < disableOnDeath.Length; i++) {
			disableOnDeath [i].enabled = false; 
		}

		//special case to handle the collider 
		Collider col = GetComponent<Collider> ();
		if (col != null) {
			col.enabled = false;
		}

		//call respawn method
		//Coroutine is executed until an yield instruction is found, its becuase of the time thing (3 seconds)
		StartCoroutine (Respawn ()); 
			

	}

	//this is the respawn method 
	private IEnumerator Respawn()
	{
		//to find respawn time, it is stored in the match setting class 
		yield return new WaitForSeconds (GameManager.singleton.matchsettings.respawnTime); // wait for 3 seconds. 
		setDefaults(); //reset all the defaults - will enable components again

		//TODO: check to make sure the network start position is there

		//this will return one of the spawn point registered in the network manager 
		Transform _spawnPoint = NetworkManager.singleton.GetStartPosition();
		transform.position = _spawnPoint.position;
		transform.rotation = _spawnPoint.rotation;

		Debug.Log ("Player respawned");

	}


}
