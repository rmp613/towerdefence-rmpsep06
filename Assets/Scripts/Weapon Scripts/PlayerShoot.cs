﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent (typeof(WeaponManger))]
public class PlayerShoot : NetworkBehaviour
{

	private BaseWeapon CurrentWeapon;

	private WeaponManger weaponManager;

	//Note: I can make the gun on a seperate layer, then have it rendered by another camera
	//and have that camera go ontop of the current camera
	//what this would do is basicly prevent the gun clipping in walls - dont know if its worth it yet
	//this would also mean I can make the gun look very close - like in FPS games
	//while the model could hold the gun at a more reasonable range

	//convention for tags
	private const string PLAYER_TAG = "Player";

	private const string ENEMY_TAG = "Enemy";


	//control what we hit - for example freindly players.
	[SerializeField] 
	private LayerMask mask;



	// Use this for initialization
	void Start ()
	{
		//Debug.Log ("The gun is active");
		if (!isLocalPlayer) {
			return; 
		} 

		weaponManager = GetComponent<WeaponManger> (); 
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Debug.Log ("Testing for local player...");	

		if (!isLocalPlayer) {
			//Debug.Log ("No network idenity found or is not the right player.");
			return; //this is stopping th ray cast from being called...
		} else {
			CurrentWeapon = weaponManager.GetCurrentWeapon (); 

			if (CurrentWeapon.fireRate <= 0f) {
				
				//If mouse is clicked and there is enough ammo
				if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0) {
					//shoot method casts the raycast
					Shoot ();

				} else {
					//The stats code
					Statistics.timeWithoutShots += Time.deltaTime;
					if (Statistics.timeWithoutShots > 5) {
						Statistics.shotsFired = 0;
					}
				}
			} else {
				if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0) {
					//shoot method casts the raycast
					InvokeRepeating ("Shoot", 0f, 1f / CurrentWeapon.fireRate);
					//This will make the player shoot more than once per click
					//this will start at 0f: immiedatly 
					// the spurt of the bullets is = 1 / the fire rate 
				} else if (Input.GetButtonUp ("Fire1")) {
					//when you relase the mouse button it should stop the invoking of that method. 
					CancelInvoke ("Shoot");
				}

			}
		}
	}

	BasicEnemy LastEnemyHit;

	//this is a local method so it is marked as client
	[Client]
	void Shoot ()
	{
		Debug.Log ("SHOTS FIRED");
		RaycastHit shot; //the out variable that will hold information from the hit of the ray cast

		if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out shot, CurrentWeapon.maxRange, mask)) {
			//if ray hit then this will be called. 
			Debug.Log ("The shot has hit " + shot.collider.name); 

			//so here we are checking if we hit another player, if so then it can detect though looking
			//at the player tag or the layer "local/remote layer" or a few other things, but for now this should
			//be ok
			if (shot.collider.tag == PLAYER_TAG) {
				//the client tells the server that it hit the player
				//also passes the weapon damage 
				CmdPlayerShot (shot.collider.name, CurrentWeapon.damage);
			}
			else if (shot.collider.tag == ENEMY_TAG)
			{
				LastEnemyHit = shot.collider.GetComponent<BasicEnemy>();

				//TODO: ok so I learnt that the CMD network functions can only take primitives, so I cant pass game objects to the server
				//which means in order to destroy an object on the server it must have some kind or ID 
				//I will have to set up an ID system for enemies, when they spawn, I can just change their game object name to an ID which increments. 

				/*
				if (Network.isServer) {
					enemy.RpcTakeDamage (CurrentWeapon.damage); 
				} */

				CmdEnemyShot("enemy", CurrentWeapon.damage);
			}
		}

		Statistics.timeWithoutShots = 0;
		Statistics.shotsFired += 1;
	}

	/*
	 * Cmd is a command, a method called only on the server.  
	 * 
	*/
	[Command]
	void CmdPlayerShot (string _playerID, int _damage)
	{
		//the server decided what to do with the client informaiton about the hit. 
		Debug.Log (_playerID + " has been shot.");
		//GameObject.Find (_ID); // using the ID from the hit, we can find the game object
		//GameObject.Find  is slow so it is better to use a player dictionary which i put in game manger

		//this will find the player hit and return the player
		Player _player = GameManager.GetPlayer (_playerID); 
		//now we can give that player damage. 
		_player.RpcTakeDamage (_damage); 

	}

	[Command]
	void CmdEnemyShot (string enemy, int _damage) //BasicEnemy
	{
		Debug.Log ("Enemy has been shot.");
		LastEnemyHit.RpcTakeDamage (_damage); 
	}
}
