﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShootingSystem : MonoBehaviour {

    public float fireRate;
    public float attackSpeed;
    public bool beam;
    public int damage;
    public List<GameObject> projectileSpawns;
    public GameObject projectile;
    public GameObject target;
    public float fieldOfView;
    float mfireTimer = 0.0f;

    List<GameObject> mlastProjectile = new List<GameObject>();

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        target = gameObject.GetComponent<TowerTracking>().target;
        if (gameObject.GetComponent<TowerTracking>().trackCount > 0)
        {
            {
                if (beam && mlastProjectile.Count <= 0)
                {
                    //float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
                    float angle = Quaternion.Angle(transform.FindChild("Pivot").rotation, Quaternion.LookRotation(target.transform.position - transform.position));

                    if (angle < fieldOfView)
                    {
                        SpawnProjectiles();
                    }

                }
                else if (beam && mlastProjectile.Count > 0)
                {
                    //float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
                    float angle = Quaternion.Angle(transform.FindChild("Pivot").rotation, Quaternion.LookRotation(target.transform.position - transform.position));

                    if (angle > fieldOfView)
                    {
                        while (mlastProjectile.Count > 0)
                        {
                            Destroy(mlastProjectile[0]);
                            mlastProjectile.RemoveAt(0);
                        }
                    }
                }
                else
                {
                    mfireTimer += Time.deltaTime;

                    if ((mfireTimer >= fireRate) && target!=null)
                    {
                        //float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
                        float angle = Quaternion.Angle(transform.FindChild("Pivot").rotation, Quaternion.LookRotation(target.transform.position - transform.position));

                        if (angle < fieldOfView)
                        {
                            SpawnProjectiles();

                            mfireTimer = 0.0f;
                        }
                    }
                }
            }
        }
	}

    void SpawnProjectiles()
    {
        if (!projectile)
        {
            return;
        }

        mlastProjectile.Clear();
        for (int i = 0; i < projectileSpawns.Count; i++)
        {
            if(projectileSpawns[i])
            {
                GameObject proj = Instantiate(projectile, projectileSpawns[i].transform.position, Quaternion.Euler(projectileSpawns[i].transform.forward)) as GameObject;
                proj.GetComponent<BaseProjectile>().FireProjectile(projectileSpawns[i], target, damage, attackSpeed);
            }
        }
    }
}
